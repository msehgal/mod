<%-- 
    Document   : my_music
    Created on : Mar 24, 2013, 9:25:19 PM
    Author     : I
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>My Music</title>
<link href="CSS/mymusic.css" rel="stylesheet" type="text/css" />
<link href="CSS/style1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
<div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
	</div>
	<div class="main_content">
		<div class="logo">Search Logo</div>
	
<div id="playlist_list" >
<table    >
<tr><td><a >Nominated Playlist</a></td></tr>
<tr><td><a >Playlist1</a></td></tr>
<tr><td><a >Playlist2</a></td></tr>
<tr><td><a>Playlist3</a></td></tr>
<tr><td><a >Playlist4</a></td></tr>
<tr><td><a>Playlist5</a></td></tr>
<tr><td><a>Create Playlist+</a></td></tr>
</table>
</div>
<div class="playlist_header">

<b>&nbsp;&nbsp;Playlist Name</b><br />
<table>
<tr>
	<td>&nbsp;</td><td><a>Play</a></td><td><a>Add Songs</a></td><td><a>Remove Songs</a></td>
</tr>
</table>
<br />
</div>
<div class="playlist_content">
<table>
	<tr><th max-width="50px"></th><th>Title</th><th>Artist</th><th>Album</th><th>On Your Mind</th><th>Dedicate</th><th>Recommend</th></tr>
	<tr><td max-width="50px"></td><td>Pyaar Ki Daastan</td><td>Amit Paul</td><td>Luck By Chance</td><td>On Your Mind</td><td>Dedicate</td><td>Recommend</td></tr>
	<tr><td max-width="50px"></td><td>Aap Ki Kashish</td><td>Himesh Reshammiya</td><td>Aashiq Banaya Aapne</td><td>On Your Mind</td><td>Dedicate</td><td>Recommend</td></tr>
</table>
</div>
</div>
<div class="footer">Playlist Controls will Come here.</div>
</div>
</body>
</html>
