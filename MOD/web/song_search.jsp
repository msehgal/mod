<%-- 
    Document   : song_search
    Created on : Mar 24, 2013, 9:34:08 PM
    Author     : I
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>MOD Song Search</title>
        <link href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/song_search.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
	</div>
	<div class="main_content">
		<div class="logo">Search Logo</div>
</div>
<div class="search_field">
<form>
	<input type="text" size="50" /><input type="submit" value="Search" />
</form>
</div>
<div class="search_result_content" >
	
	<table>
		<tr>
		<th>&nbsp;</th><th>Title</th><th>Artist</th><th>Album</th><th>Ratings</th><th>Playcount</th><th>Chart Position</th>
		</tr>
		<tr>
		<td>1</td><td>Chandigarh Vaaliye</td><td>Sherry Mann</td><td>Sone Di Chiri</td><td>4.5</td><td>3</td><td>-</td>
		</tr>
	</table>
</div>
<div class="footer">&nbsp;</div>
</div>
    </body>
</html>
