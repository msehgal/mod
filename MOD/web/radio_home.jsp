<%-- 
    Document   : radio_home
    Created on : Mar 24, 2013, 9:01:07 PM
    Author     : Mohit
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Music On Demand</title>
        <link  href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/home_radio.css" rel="stylesheet" type="text/css" />
        <script>
            function setLinkAtribute(songID)
            {
            }
        </script>
    </head>
     <%
    Connection con=DBConnector.getConnection();
    String toplist_query="select * from song s left outer join playlist_contains_song p on s.songID=p.songID where p.playlistID=1;";
    Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
    
    ResultSet toplist_rs=stmt.executeQuery(toplist_query);
   
    application.setAttribute("playlist_rs", toplist_rs);
    PreparedStatement artistStmt=null;
    String artist_query="select dbo.artist.artist_name from artist where songID= ?;";
    artistStmt=con.prepareStatement(artist_query,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
    application.setAttribute("artistPreparedStmt", artistStmt);
    ResultSet artist_rs=null;
     int count=1;
%>
    <body>
       <div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
                        <div class="top_navigation_bar_element" ><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                        }else{out.println("&nbsp;");} out.flush(); %> </div>
                        <div class="top_navigation_bar_element">
                            <html:link action="/logout" >Logout </html:link> </div>
	</div>
	<div class="main_content">
		<div class="logo"><h1 align="left" >&nbsp;&nbsp;MUSIC ON DEMAND</h1></div>
                <%
                if( session.getAttribute("email_id")==null){
                %>
                <html:form action="login" >
	<div class="login_container" >
		
		<div class="login_content">
			Username:<br/>
			<input type="text" name="name" />
			<br/>
			Password:<br/>
			<input type="password" name="password" />
			<br />
			<a style="text-align:left">Forgot Password</a><br/>
			<a style="text-align:right">Log In</a><br />
                        <input type="submit" value="Sign In" /><a style="text-align:left">Sign In</a>
		</div>
                
	</div>
                </html:form>
	<%}%>
       
	<div class="radio">
			
			<div class="radio_head"><h2 align="center" >Top List</h2></div>
			<div class="radio_content">
			<table>
                           
				<tr>
					<th>&nbsp;</th><th>Song Name</th><th>Artist</th><th>Album</th>
				</tr>
                                                                <%
               
                while(toplist_rs.next()){
                artistStmt.setInt(1, Integer.parseInt(toplist_rs.getString("songID")));
                artist_rs=artistStmt.executeQuery();
                                                                %>
				<tr>
                                    <td> <%=count++%></td><td><a href="view_song.do?songID=<%= toplist_rs.getString("songID") %>" style="color: black" ><%=toplist_rs.getString("name") %></a></td>
                                    <td>
                                        <%
                                        
                                            while(artist_rs.next())
                                            {%>
                                            <%=artist_rs.getString("artist_name")%>
                                           <% }
                                        %>
                                    </td>
                                    <td>
                                        <%=toplist_rs.getString("album") %>
                                    </td>
				</tr>
                                <% 
                } 
                      /*this loop acts as padding i.e. handles design if there are too less songs in toplist*/
                while(count++<=15){      %>     
                                <tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
                                <%}%>
				
			</table>
			</div>
		</div>
	</div>
                                <div class="footer"><a href="MusicPlay.jsp" target="_music" >Play It!</a></div>
</div>
</body>
    </body>
</html>
