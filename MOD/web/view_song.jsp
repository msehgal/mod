<%-- 
    Document   : view_song
    Created on : Mar 24, 2013, 9:38:27 PM
    Author     : Mohit Sehgal
--%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    
    if(request.getParameter("c")!=null&&request.getParameter("c").equals("true"))
    {
        String comment= request.getParameter("comment");
        Connection con=DBConnector.getConnection();
        Statement stmt=con.createStatement();
        Statement stmt2=con.createStatement();
        String userID=null;
        if(session.getAttribute("userID")==null)
        {
            userID="-1";
        }
        else
        {
            userID=(String)session.getAttribute("userID");
        }
        String query="INSERT INTO [music].[dbo].[song_review]([reviewText],[songID],[userID])VALUES('"+comment+"' ,"+request.getParameter("songID")+","+userID+ " );";
        stmt.executeUpdate(query);
        ResultSet rev=stmt2.executeQuery("select * from song_review sr inner join [dbo].[user] usr on sr.userID=usr.userID WHERE sr.songID="+request.getParameter("songID"));
        request.setAttribute("viewSongReview_rs", rev);
       
    } 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Song- <%= request.getAttribute("viewSongName") %></title>
<link href="CSS/style1.css" rel="stylesheet" type="text/css" />
<link href="CSS/view_song.css" rel="stylesheet" type="text/css" />
<script>
    function submitOnEnter(inputElement, event) {  
        if (event.keyCode === 13) { // No need to do browser specific checks. It is always 13.  
            inputElement.form.submit();  
        }  
    }  
    function cleartextarea(element)
    {
        element.value='';
    }
</script>
    </head>
    <body>
       
<div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
	</div>
	<div class="main_content">
		<div class="logo">View Song Logo</div>
	</div>
	<div class="song_header">
	<table align="center">
	<tr><td colspan="4" >
	<h3><%= request.getAttribute("viewSongName") %></h3></td></tr>
	<tr><td colspan="4" ><b>ARTIST(S)</b>: <% 
            ResultSet artist=(ResultSet)request.getAttribute("viewSongArtist_rs");
            artist.first();
                out.println(artist.getString("artist_name"));
        while(artist.next())
        {
            out.println(", "+artist.getString("artist_name"));
        }
        ResultSet song=(ResultSet)request.getAttribute("viewSong_rs");
        song.first();
        %></td></tr>
        <tr><td align="left" colspan="2"><b>ALBUM</b>:&nbsp;<%= song.getString("album") %></td><td align="left" colspan="2">
                <b>COMPOSER</b>: <%= song.getString("music_director") %></td></tr>
        <tr><td align="left" colspan="2"><b>PLAYCOUNT</b>:&nbsp;<%= song.getString("play_count") %></td><td align="left" colspan="2">&nbsp;<b>RATING</b>&nbsp;<%= song.getString("rating") %></td></tr>
	<tr><td><a>Add to Playlist</a></td><td> <a>Recommend to circle</a> </td><td> <a>Set as On Your Mind</a></td><td><a>Dedicate</a></td>
	</table>
	</div>
	<div class="review_content">
	<h4>Reviews</h4>
	<%
            ResultSet review=(ResultSet)request.getAttribute("viewSongReview_rs");
            while(review.next())
            {
                String userID=review.getString("userID");
             
        %>
        <div class="comment" >
            <%= review.getString("reviewText") %>
            <a style="text-align: right "> -- <%= review.getString("user_name") %></a>
            <!--Link to user profile!-->
        	</div>
        <% } %>
        <div class="comment" >
            <form action="view_song.do?songID=<%=request.getParameter("songID") %>" method="post">
            <textarea rows="3" cols="80" name="comment" id="comment_text_area" onkeypress="submitOnEnter(this, event);" onclick="return cleartextarea(this);"  >
	leave your comment
	</textarea>
            <br>
            <input type="submit" value="Comment"/>
            <input type="hidden" name="c" value="true"/>
            </form>
        </div>

	</div>
</div>
<%

                 request.setAttribute("viewSong_rs", (ResultSet)request.getAttribute("viewSong_rs"));
                 request.setAttribute("viewSongArtist_rs", (ResultSet)request.getAttribute("viewSongArtist_rs"));
                 request.setAttribute("viewSongReview_rs", (ResultSet)request.getAttribute("viewSongReview_rs"));
%>

    </body>
</html>
