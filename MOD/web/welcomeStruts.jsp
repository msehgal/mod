<%-- 
    Document   : radio_home
    Created on : Mar 24, 2013, 9:01:07 PM
    Author     : Mohit
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
    Connection con=DBConnector.getConnection();
    String toplist_query="select * from song s left outer join playlist_contains_song p on s.songID=p.songID where p.playlistID=1;";
    Statement stmt=con.createStatement();
    ResultSet toplist_rs=stmt.executeQuery(toplist_query);
    PreparedStatement artistStmt=null;
    String artist_query="select dbo.artist.artist_name from artist where songID=?;";
    
%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Music On Demand</title>
        <link  href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/home_radio.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
       <div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
                        <div class="top_navigation_bar_element" ><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                        }else{out.println("&nbsp;");} out.flush(); %> </div>
                        <div class="top_navigation_bar_element">
                            <html:link action="/logout" >Logout </html:link> </div>
	</div>
	<div class="main_content">
		<div class="logo"><h1 align="left" >&nbsp;&nbsp;MUSIC ON DEMAND</h1></div>
                <%
                if( session.getAttribute("email_id")==null){
                %>
                <html:form action="login" >
	<div class="login_container" >
		
		<div class="login_content">
			Username:<br/>
			<input type="text" name="name" />
			<br/>
			Password:<br/>
			<input type="password" name="password" />
			<br />
			<a style="text-align:left">Forgot Password</a><br/>
			<a style="text-align:right">Log In</a><br />
                        <input type="submit" value="Sign In" /><a style="text-align:left">Sign In</a>
		</div>
                
	</div>
                </html:form>
	<%}%>
	<div class="radio">
			
			<div class="radio_head"><h2 align="center" >Top List</h2></div>
			<div class="radio_content">
			<table>
                           
				<tr>
					<th>&nbsp;</th><th>Song Name</th><th>Artist</th><th>Album</th>
				</tr>
                                     
                                <%
                int count=1;
                while(toplist_rs.next()){%>
				<tr>
                                    <td> <%=count++%></td><td><%=toplist_rs.getString("title") %></td><td>Singer1</td><td>album/movie</td>
				</tr>
                                <% } %>
				<tr>
					<td>2</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>3</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>4</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>5</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>6</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>7</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>8</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>9</td><td>Song Name</td><td>Singer1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>10</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>11</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>12</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>13</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>14</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
				<tr>
					<td>15</td><td>Song Name</td><td>Artist1</td><td>album/movie</td>
				</tr>
			</table>
			</div>
		</div>
	</div>
	<div class="footer">Radio Controls will come here</div>
</div>
</body>
    </body>
</html>
