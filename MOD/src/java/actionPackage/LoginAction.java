/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import formPackage.LoginActionForm;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author I
 */
public class LoginAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "login_success";
    private static final String ERROR = "login_error";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LoginActionForm login=(LoginActionForm)form;
         //String enteredUser=login.getName();
        String enteredPassword=login.getPassword();
        String enteredEmail=login.getName();
        System.out.println("hello");
        HttpSession session=request.getSession();
        Connection con=DBConnector.getConnection();
        Statement stat=con.createStatement();
        ResultSet rs=stat.executeQuery("SELECT * FROM [user] where email_id='"+enteredEmail+"' ;");
        if(rs==null)
            mapping.findForward(ERROR);
        while(rs.next())
        {
            String tmp=rs.getString("email_id");
            String tmp2=rs.getString("password");
            String tmp3=rs.getString("user_name");
            System.out.println("tmp3"+tmp3);
            if(tmp.equals(enteredEmail)&&tmp2.equals(enteredPassword))
            {
                session.setAttribute("email_id", enteredEmail);
                session.setAttribute("password", enteredPassword);
                session.setAttribute("name", tmp3);
                session.setAttribute("userID", rs.getString("userID"));
                return mapping.findForward(SUCCESS);
            }
        }
        
        return mapping.findForward(ERROR);
        
    }
}
