<%-- 
    Document   : sign_up
    Created on : 19 May, 2013, 2:45:03 PM
    Author     : MOHIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <script>
function validateForm1()
{
var x=document.forms["firstname"]["fname"].value;
if (x==null || x=="")
  {
  alert("Credentials with * must be filled");
  return false;
  }
}

function validateForm2()
{
var x=document.forms["email"]["em"].value;
if (x==null || x=="")
  {
  alert("Credentials with * must be filled");
  return false;
  }
}

function validateForm()
{
var x=document.forms["emailform"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert("Please provide valid e-mail address");
  return false;
  }
}
    </script>
    <head>
        <title>Music On Demand</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
       <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>

<div id="main">
    
<div>
    <%
        String status=(String)session.getAttribute("add_user_status");
        String exist=(String)session.getAttribute("exists");
        //out.println("status "+status);
    if(exist.equals("no")&&status.equals("success"))
    {
        %>
        You Have Successfully Registered as our User. Kindly Click <a href="login.jsp">here</a> to Login.
    
    <%
    }
    else
    {
        if(session.getAttribute("failed_mail")!=null){
        %>
        Sorry Your Registration Failed. <%=session.getAttribute("failed_mail") %> is already registered.Please Try Again <a href="sign_up.jsp">here</a>. 
        <%
        }
        else
        {
            %>
            Sorry, Error Occurred. Please Try Again <a href="sign_up.jsp">here</a>. 
        
        <%
        }
    }
    session.invalidate();
    %>
</div> 
<!-- END of slider -->
  <div class="cleaner h50"></div>
 <div class="cleaner"></div>
</div> 

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
              <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
</body>
</html>


