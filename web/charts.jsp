<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>MOD Charts</title>
		<link href="CSS/style1.css" rel="stylesheet" type="text/css" />
		<link href="CSS/charts.css" rel="stylesheet" type="text/css" />
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="http://java.com/js/dtjava.js"></SCRIPT>

    </head>
     <%
         ResultSet alltime_charts_rs=(ResultSet)request.getAttribute("alltime_charts_rs");
         
     int count=1;
%>
    <body>
   <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp" class="ddsmoothmenu">Radio</a></li>
                <li><a href="charts.do" class="selected">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
<div id="middle_wrapper_radio" ></div>
<div id="main">		
<div class="chart_mode">
<!--<table><tr><td width="100px"><a>Weekly</a></td><td width="100px"><a>All Time</a></td></tr></table>!-->
</div>
<div class="result_content">
  <table width="1108">
    <tr>
      <th width="178">Chart Position</th>
      <th width="178">Title</th>
      <th width="178">Album</th>
      <th width="178">Artist</th>
      <th width="178">&nbsp;</th>
      <th width="178">&nbsp;</th>
    </tr>
    <%
                    while( alltime_charts_rs.next())
                    {
                        String songID=alltime_charts_rs.getString("songID");
                        String title=alltime_charts_rs.getString("name");
                        String album=alltime_charts_rs.getString("album");
                        %>
    <tr>
      <td><%=count++%></td>
      <td><a href="view_song.do?songID=<%=songID%>"><%=title%></a></td>
      <td><%=album%></td>
      <%
                        String artist=alltime_charts_rs.getString("artist_name");
                        while(alltime_charts_rs.next()&&alltime_charts_rs.getString("songID").equals(songID))
                        {
                            artist+=(","+alltime_charts_rs.getString("artist_name"));
                        }
                        alltime_charts_rs.previous();
                        %>
      <td><%=artist%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <%
                    }
                %>
  </table>
</div>
         <div class="cleaner"><h3 style="text-align: center" ><a href="musicplay.do?plid=charts" target="_music" >Play It!</a></h3></div>

</div>
<div class="cleaner"></div>
</div> 

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="musicplay.do">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
              <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
</body>
</html>
