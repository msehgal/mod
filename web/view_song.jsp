<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if(request.getParameter("v")!=null&&request.getParameter("v").equals("true"))
    {
        int vote=Integer.parseInt(request.getParameter("vote"));
        String vote_query= "UPDATE [music].[dbo].[song]  SET [rating] = [rating]+"+vote+" ,[rated_times] = [rated_times]+1 WHERE songID='"+request.getParameter("songID")+"'";
        Connection con=DBConnector.getConnection();
        Statement stmt=con.createStatement();
        stmt.execute(vote_query);
        //RequestDispatcher disp=request.getRequestDispatcher("view_song.jsp?songID="+request.getParameter("songID"));
        //disp.forward(request, response);
    }
    if(request.getParameter("c")!=null&&request.getParameter("c").equals("true"))
    {
        String comment= request.getParameter("comment");
        Connection con=DBConnector.getConnection();
        Statement stmt=con.createStatement();
        Statement stmt2=con.createStatement();
        String userID=null;
        if(session.getAttribute("userID")==null)
        {
            userID="-1";
        }
        else
        {
            userID=(String)session.getAttribute("userID");
        }
        String query="INSERT INTO [music].[dbo].[song_review]([reviewText],[songID],[userID])VALUES('"+comment+"' ,"+request.getParameter("songID")+","+userID+ " );";
        stmt.executeUpdate(query);
        ResultSet rev=stmt2.executeQuery("select * from song_review sr inner join [dbo].[user] usr on sr.userID=usr.userID WHERE sr.songID="+request.getParameter("songID"));
        request.setAttribute("viewSongReview_rs", rev);
       
    }
    
    ResultSet playlistList_rs=(ResultSet)session.getAttribute("playlistList_rs");
    ResultSet friendList_rs=(ResultSet)session.getAttribute("friendList_rs");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Song- <%= request.getAttribute("viewSongName") %></title>
        <link href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/view_song.css" rel="stylesheet" type="text/css" />
        <link href="CSS/drop_down.css"  rel="stylesheet" type="text/css" />
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">

<!--plugin specific imports -->
<script src='js/rating/jquery.js' type="text/javascript"></script>
<script src='js/rating/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
 <script src='js/rating/jquery.rating.js' type="text/javascript" language="javascript"></script>
 <link href='js/rating/jquery.rating.css' type="text/css" rel="stylesheet"/>

<script>
    function submitOnEnter(inputElement, event) {  
        if (event.keyCode === 13) { // No need to do browser specific checks. It is always 13.  
            inputElement.form.submit();  
        }  
    }  
    function cleartextarea(element)
    {
        element.value='';
    }
</script>
    </head>
    <body>
       
<div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
<div id="main">
    <div class="song_header">
	<table align="center">
	<tr><td colspan="4" >
	<h3><%= request.getAttribute("viewSongName") %></h3></td></tr>
	<tr><td colspan="4" ><b>ARTIST(S)</b>: <% 
            ResultSet artist=(ResultSet)request.getAttribute("viewSongArtist_rs");
            artist.first();
                out.println(artist.getString("artist_name"));
        while(artist.next())
        {
            out.println(", "+artist.getString("artist_name"));
        }
        ResultSet song=(ResultSet)request.getAttribute("viewSong_rs");
        song.first();
        %></td></tr>
        <tr><td align="left" colspan="2"><b>ALBUM</b>:&nbsp;<%= song.getString("album") %></td><td align="left" colspan="2">
                <b>COMPOSER</b>: <%= song.getString("music_director") %></td></tr>
        <tr><td align="left" colspan="2"><b>PLAYCOUNT</b>:&nbsp;<%= song.getString("play_count") %></td><td align="left" colspan="2">&nbsp;<b>RATING</b>&nbsp;<%= (int)(Double.parseDouble(song.getString("rating"))/Integer.parseInt(song.getString("rated_times")))%></td></tr>
       
        <form action="view_song.do?songID=<%=request.getParameter("songID")%>" method="post" >
        <tr> 
            <td><b>Rate Now: </b></td>
            <td>
                <%
                    int rating=(int)(Double.parseDouble(song.getString("rating"))/Integer.parseInt(song.getString("rated_times")));
                    int i=0;
                    for(i=1;i<=rating-1;i++)
                    {
                        
                %>
                        <input name="vote" type="radio" value="<%=i%>"   title="<%=i%>/10"   class="star {split:2}"/>
                <%
                       }
                %>
                
                    <input name="vote" type="radio" value="<%=i%>" title="<%=i%>/10" class="star {split:2}" checked="checked"/>
                   <%
                       for(i=rating+1;i<=10;i++)
                    {
                        
                %>
                        <input name="vote" type="radio" value="<%=i%>"   title="<%=i%>/10"   class="star {split:2}"/>
                <%
                       }
                %>
                    
                    
            </td>
            
        </tr>
        <tr><td > <input type="submit" value="Vote"/>
            <input type="hidden" name="v" value="true"/> </td><td>&nbsp;</td></tr>
         </form>
        <tr>
            <td>
                <ul>
                    <li> 
                        <a href="#" style="color: #F3F3F3;" >Add to Playlist</a>
                        <ul>
                            <%
                                String plName="",plId="";
                                System.out.println("playlist rs "+playlistList_rs);
                                if(playlistList_rs!=null&&playlistList_rs.first())
                                while(playlistList_rs.next())
                                {
                                    plName=playlistList_rs.getString("playlist_name");
                                    plId=playlistList_rs.getString("playlistID");
                                
                            %>
                            
                            <li> <a style="color: #F3F3F3;" href="addsong.do?pl=<%=plId%>&songID=<%=request.getParameter("songID")%>&src=view" ><%=plName%> </a></li>
                            <%
                                }
                            %>
                        </ul>
                    </li>
                
                </ul>
            </td>
            
        <td>
            <ul>
                    <li> 
                        <a style="color: #F3F3F3;" href="recommend.do?songID=<%=request.getParameter("songID")%>&src=view" >Recommend To Circle</a>
                       
                    </li>
                
                </ul>
        </td>
        <td>
            <ul>
                    <li> 
                        <a style="color: #F3F3F3;" href="setonmind.do?songID=<%=request.getParameter("songID")%>&src=view" >On Your Mind</a>
                       
                    </li>
                
                </ul>
     
        </td>
        <td>
            
                <ul>
                    <li> 
                        <a style="color: #F3F3F3;" href="#" >Dedicate</a>
                        <ul>
                            <%
                                String frName="",frndId="";
                                System.out.println("friendlist rs "+friendList_rs);
                                if(friendList_rs!=null&&friendList_rs.first())
                                while(friendList_rs.next())
                                {
                                    frName=friendList_rs.getString("user_name");
                                    frndId=friendList_rs.getString("usrfrndID");
                                    
                            %>
                            
                            <li> <a style="color: #F3F3F3;" href="dedicate.do?frnusr=<%=frndId%>&songID=<%=request.getParameter("songID")%>&src=view" ><%=frName%> </a></li>
                            <%
                                }
                            %>
                        </ul>
                    </li>
                
                </ul>
        </td>
        </tr>
        <tr><td><form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<%= session.getAttribute("email_id")%>">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="item_name" value="<%= request.getAttribute("viewSongName")%>" >
<input type="hidden" name="amount" value="12.99">
<input type="image" src="http://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
               </form></td><td>&nbsp;</td></tr>
        </table>
	</div>
	<div class="review_content">
	<h4>Reviews</h4>
	<%
            ResultSet review=(ResultSet) request.getAttribute("viewSongReview_rs");
            while(review.next())
            {
                String userID=review.getString("userID");
             
        %>
        <div class="comment" >
            <%= review.getString("reviewText") %>
            <a style="text-align: right "> -- <%= review.getString("user_name") %></a>
            <!--Link to user profile!-->
        	</div>
        <% } %>
        <div class="comment" >
            <form action="view_song.do?songID=<%=request.getParameter("songID") %>" method="post">
            <textarea rows="3" cols="80" name="comment" id="comment_text_area" onkeypress="submitOnEnter(this, event);" onclick="return cleartextarea(this);"  >
	leave your comment
	</textarea>
            <br>
            <input type="submit" value="Comment"/>
            <input type="hidden" name="c" value="true"/>
            </form>
        </div>

	</div>
</div>
<%

                 request.setAttribute("viewSong_rs", (ResultSet)request.getAttribute("viewSong_rs"));
                 request.setAttribute("viewSongArtist_rs", (ResultSet)request.getAttribute("viewSongArtist_rs"));
                 request.setAttribute("viewSongReview_rs", (ResultSet)request.getAttribute("viewSongReview_rs"));
%>

  <div class="cleaner h50"></div>
  <div class="cleaner"></div>



    </body>
</html>
