
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Music On Demand</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
<%
    Connection con=DBConnector.getConnection();
    String toplist_query="select * from song s left outer join playlist_contains_song p on s.songID=p.songID where p.playlistID=1;";
    Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
    
    ResultSet toplist_rs=stmt.executeQuery(toplist_query);
   
    application.setAttribute("playlist_rs", toplist_rs);
    PreparedStatement artistStmt=null;
    String artist_query="select dbo.artist.artist_name from artist where songID= ?;";
    artistStmt=con.prepareStatement(artist_query,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
    application.setAttribute("artistPreparedStmt", artistStmt);
    ResultSet artist_rs=null;
     int count=1;
%>
    </head>

<body>

<div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp" class="selected">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
  <div class="middle_wrapper" ><img src="images/back1.jpg" width="1247" height="320" /></div>
 
<div id="main">

<table align="center" width="898" height="114">
                           
				<tr>
					<th align="left"width="15">S.No</th><th align="left" width="66">Song Name</th><th align="left" width="50">Artist</th><th align="left"width="37">Album</th>
				</tr>
                                                                <%
               
                while(toplist_rs.next()){
                artistStmt.setInt(1, Integer.parseInt(toplist_rs.getString("songID")));
                artist_rs=artistStmt.executeQuery();
                                                                %>
				<tr>
                                    <td> <%=count++%></td><td><a href="view_song.do?songID=<%= toplist_rs.getString("songID") %>" style="color: black"  ><%=toplist_rs.getString("name") %></a></td>
                                    <td>
                                        <%
                                        
                                            while(artist_rs.next())
                                            {%>
                                            <%=artist_rs.getString("artist_name")%>
                                           <% }
                                        %>
                                    </td>
                                    <td>
                                        <%=toplist_rs.getString("album") %>
                                    </td>
				</tr>
                                <% 
                } 
                      /*this loop acts as padding i.e. handles design if there are too less songs in toplist*/
                while(count++<=15){      %>     
                                <tr>
					<td align="right">&nbsp;</td><td align="right">&nbsp;</td><td align="right">&nbsp;</td><td align="right">&nbsp;</td>
				</tr>
                                <%}%>
				
			</table>
</div> 



                                <div class="cleaner"><h3 style="text-align: center" ><a href="musicplay.do?plid=1" target="_music" >Play It!</a></h3></div>

<div class="cleaner"></div>
</div> 

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                    <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
    </body>
</html>
