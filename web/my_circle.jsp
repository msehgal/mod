<%-- 
    Document   : my_home
    Created on : Mar 24, 2013, 9:22:54 PM
    Author     : Mohit
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Home</title>
        <link href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/myhome.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <body>
<%
    if(session.getAttribute("name")==null)
    {
        response.sendRedirect("/MOD/radio_home.jsp");
    }
    String md=request.getParameter("md");
    if(md==null)
        md="1";
    String onMind="select * from song where songID=(select songID from [dbo].[user] where userID="+session.getAttribute("userID")+")";
    Connection con=DBConnector.getConnection();
    Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    ResultSet onMind_rs=stmt.executeQuery(onMind);
    String onMindSong;
    if(onMind_rs.next())
    {
        onMindSong=onMind_rs.getString("name");
        onMindSong=onMindSong==null?"":onMindSong;
    }
    else
    {
        onMindSong="";
    }
    //recommended
    Statement stmt2=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    String recommended_query="select usr.user_name ,rec.* from (select userID,time_recommend,r.songID ,name, album,music_director,play_count,rating,rated_times,release_date,[path]  from dbo.user_recommends_song r,dbo.song s  where r.userID  in( select dbo.user_friend_of_user.friendID2 from dbo.user_friend_of_user where friendID1="+session.getAttribute("userID")+"  ) and s.songID=r.songID ) rec join dbo.[user] usr on usr.[userID]=rec.[userID]";
    ResultSet recommended_rs=stmt2.executeQuery(recommended_query);
    
    
    //dedicated
    Statement stmt3=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    String dedicated_query="select usr2.user_name, ded.* from (select usr.friendID,usr.time_dedicated,usr.userID, s.* from dbo.user_dedicates_song usr,dbo.song s where usr.userID="+session.getAttribute("userID")+" and usr.songID=s.songID) ded join dbo.[user] usr2 on ded.friendID=usr2.userID;";
    ResultSet dedicated_rs=stmt3.executeQuery(dedicated_query);
    ResultSet circle_rs=(ResultSet)session.getAttribute("circle_rs");
%>
<div class="base_all">
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
	</div>
	<div class="main_content">
		<div class="logo" style="background-color:#191919" >Search Logo</div>
	
		<div class="user_head">
		<div class="user_name"><%= session.getAttribute("name")%></div>
	
                 <div class="on_mind_song">
			<a style="text-align:center" href="#">On My Mind Now <%=onMindSong%></a>
                      
		</div>
                        <div class="friends" ><a href="music_circle.do" >My Music Circle</a></div>
		<div class="user_search" >
		<form>
		<input type="text"  name="search_user"/><input type="submit" value="Search User" name="search_user_submit"  /> </form>
		</div>
			
	
	</div>
	
	</div>
	<div class="share_mode">
            <table><tr><td ><a href="my_home.jsp?md=1">Recommended to You</a></td><td ><a href="my_home.jsp?md=2">Dedicated to You</a></td></tr></table>
	</div>
	<div class="shared">
	
            <table width="50%">
                
                <%
                    
                        String frnd;
                        String userID=(String)session.getAttribute("userID");
                    if(circle_rs.first())
                    while(circle_rs.next())
                    {
                       frnd=circle_rs.getString("friendID1").equals(userID)?circle_rs.getString("uname2"):circle_rs.getString("uname1");
                     //  String 
                %>
                <tr><td style="color: #191919 ;font-size: 25px;font-family: sans-serif;" ><%=frnd%> </td><td style="color: #191919 ;font-size: 18px;font-family: monospace;" >Party On My Mind</td></tr>
                <%
                    }
                %>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
            </table>
        </div>
</body>
    </body>
</html>
