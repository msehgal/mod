<%-- 
    Document   : my_home
    Created on : Mar 24, 2013, 9:22:54 PM
    Author     : Mohit
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Home</title>
        <link href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/myhome.css" rel="stylesheet" type="text/css" />
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    
    </head>
    <body>
        <body>
<%
    if(session.getAttribute("name")==null)
    {
        response.sendRedirect("/MOD/home.jsp");
    }
    String md=request.getParameter("md");
    if(md==null)
        md="1";
    String onMind="select * from song where songID=(select songID from [dbo].[user] where userID="+session.getAttribute("userID")+")";
    Connection con=DBConnector.getConnection();
    Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    ResultSet onMind_rs=stmt.executeQuery(onMind);
    String onMindSong;
    if(onMind_rs.next())
    {
        onMindSong=onMind_rs.getString("name");
        onMindSong=onMindSong==null?"":onMindSong;
    }
    else
    {
        onMindSong="";
    }
    //recommended
    Statement stmt2=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    String recommended_query="select usr.user_name ,rec.* from (select userID,time_recommend,r.songID ,name, album,music_director,play_count,rating,rated_times,release_date,[path]  from dbo.user_recommends_song r,dbo.song s  where r.userID  in( select dbo.user_friend_of_user.friendID2 from dbo.user_friend_of_user where friendID1="+session.getAttribute("userID")+" and status=3 ) and s.songID=r.songID ) rec join dbo.[user] usr on usr.[userID]=rec.[userID] ";
    recommended_query+="union select usr.user_name ,rec.* from (select userID,time_recommend,r.songID ,name, album,music_director,play_count,rating,rated_times,release_date,[path]  from dbo.user_recommends_song r,dbo.song s  where r.userID  in( select dbo.user_friend_of_user.friendID1 from dbo.user_friend_of_user where friendID2="+session.getAttribute("userID")+" and status=3 ) and s.songID=r.songID ) rec join dbo.[user] usr on usr.[userID]=rec.[userID]";
    ResultSet recommended_rs=stmt2.executeQuery(recommended_query);
    
    
    //dedicated
    Statement stmt3=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    String dedicated_query="select usr2.user_name, ded.* from (select usr.friendID,usr.time_dedicated,usr.userID, s.* from dbo.user_dedicates_song usr,dbo.song s where usr.friendID="+session.getAttribute("userID")+" and usr.songID=s.songID) ded join dbo.[user] usr2 on ded.userID=usr2.userID";
    ResultSet dedicated_rs=stmt3.executeQuery(dedicated_query);
    ResultSet playlistList_rs=(ResultSet)session.getAttribute("playlistList_rs");
    ResultSet friendList_rs=(ResultSet)session.getAttribute("friendList_rs");
%>
<div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
<div id="main_top"></div>
	
<div class="user_head">
	<div class="user_name"><%= session.getAttribute("name")%></div>
	
        <div class="on_mind_song">
			On My Mind Now<a style="text-align:center" href="#"><%=onMindSong%></a>
        </div>
        <div class="friends" ><a href="music_circle.do" >My Music Circle</a></div>
</div>
	
	<div class="share_mode">
            <table width="372"><tr><td width="178" ><a href="my_home.jsp?md=1">Recommended to You</a></td><td width="178" ><a href="my_home.jsp?md=2">Dedicated to You</a></td></tr></table>
	</div>
        <%if(md.equals("2")){
                %>
                <h4 style="text-align: center">Dedicated to You</h4>
            <%
            }else{%>
            <h4 style="text-align: center">Recommended For You</h4>
                <%}%>
            
	<div class="shared">
	
            <%if(!md.equals("2"))
              while(recommended_rs.next())
          {
              %>
          <div class="shared_element">
	<a href="#" style="text-align:left"><%=recommended_rs.getString("name")%></a><spacer></spacer><span style="text-align:center">&nbsp;by&nbsp;</span><a href="#"><%=recommended_rs.getString("user_name")%></a> <br />

        <a>         Add to Playlist</a>
        
        
        
        &nbsp;&nbsp;<a>Recommend</a>&nbsp;&nbsp;<a>This song is 'On Your Mind'</a>&nbsp;&nbsp;<a>Dedicate</a>
	</div>
            <%
          }
            else
            {
                while(dedicated_rs.next())
          {
              %>
          <div class="shared_element">
	<a href="#" style="text-align:left"><%=dedicated_rs.getString("name")%></a><spacer></spacer><span style="text-align:center">&nbsp;by&nbsp;</span><a href="#"><%=dedicated_rs.getString("user_name")%></a> <br />
	<a>Add to Playlist</a>&nbsp;&nbsp;<a>Recommend</a>&nbsp;&nbsp;<a>This song is 'On Your Mind'</a>&nbsp;&nbsp;<a>Dedicate</a>
	</div>
            <%
          }
          %>
          
          <%
            }
          %>
        </div>
<div class="cleaner h50"></div>
  <div class="cleaner"></div>
</div> 


<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
              <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
</body>
</html>

