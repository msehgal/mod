<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>

<%
    ResultSet searchName_rs=(ResultSet)request.getAttribute("searchName_rs");
   String search_box=request.getParameter("search_box");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>MOD Song Search</title>
        <link href="CSS/style1.css" rel="stylesheet" type="text/css" />
        <link href="CSS/song_search.css" rel="stylesheet" type="text/css" />
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
        <script>
    function submitOnEnter(inputElement, event) {  
        if (event.keyCode === 13) { // No need to do browser specific checks. It is always 13.  
            inputElement.form.submit();  
        }  
    }  
    function cleartextarea(element)
    {
        element.value='';
    }
</script>
    </head>
    <body>
        <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
<div id="main">
<div class="search_field">
    <form action="search.do" method="post" >
        <input type="text" size="50" value="<%=search_box%>"  onkeypress="submitOnEnter(this, event);" name="search_box" /><input type="submit" value="Search" name="submit_button" />
</form>
</div>
<div class="search_result_content" >
	
	<table>
		<tr>
		<th>S:No</th><th>Title</th><th>Album</th><th>Ratings</th><th>Playcount</th>
		</tr>
	        
                <%
                    if(searchName_rs!=null)
                    searchName_rs.first();
                    int count=0;
                    if(searchName_rs!=null)
                    while(searchName_rs.next()){%>
                <tr>
                    <td><%=++count%></td><td><%=searchName_rs.getString("Name")%></td><td><%=searchName_rs.getString("album")%></td><td><%=(int)(Double.parseDouble(searchName_rs.getString("rating"))/Double.parseDouble(searchName_rs.getString("rated_times")))%></td><td><%= searchName_rs.getString("play_count")%></td>
                </tr>
                <%}%>
	</table>
</div>
<div class="cleaner"></div>
</div> 

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
              <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
</body>
</html>


