<%-- 
    Document   : sign_up
    Created on : 19 May, 2013, 2:45:03 PM
    Author     : MOHIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="java.sql.PreparedStatement"%>

<!DOCTYPE html>
<html>
    <script>
function validateForm()
{
var x=document.forms["signup"]["fname"].value;
var  y=document.forms["signup"]["email"].value;
var z=document.forms["signup"]["pwd"].value;
return validateForm3();
if (x==null || x==""||(y==null || y=="")||(z==null || z==""))
  {
  alert("Credentials with * must be filled");
  return false;
  }
}

function validateForm2()
{

  {
  alert("Credentials with * must be filled");
  return false;
  }
}

function validateForm3()
{
var x=document.forms["signup"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert("Please provide valid e-mail address");
  return false;
  }
}
    </script>
    <head>
        <title>Music On Demand</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>

 
<div id="main">

<table width="490" height="323" border="0" align="center">
    <form name="signup" action="signup.do" method="post" onsubmit="return validateForm();" > 
  <tr>
    <td width="156"><strong>FOR NEW USERS</strong></td>
    <td width="168">&nbsp;</td>
    <td width="152">&nbsp;</td>
  </tr>
    <tr>
    <td>First Name*</td>
    <td>
	<input  type="text" name="fname"/>
    </td>
    <td>&nbsp;</td>
	<tr>
    <td>Last Name</td>
    <td><input type="text" name="lname" /></td>
    <td>&nbsp;</td>
 	</tr>
 	<tr>
 	<td>Gender</td>
    <td><input type="radio" name="gender" value="1"/>Male</td>
    <td><input type="radio" name="gender" value="0"/>Female</td>
    </tr>
 	<tr>
    <td>Email*</td>
    <td><input type="text" name="email"></td>
    <td>&nbsp;</td>
    </tr>

  	<tr>
    <td>Password*</td>
    <td> 
    <input type="password" name="pwd" /></td>
    <td>&nbsp;</td>
    </tr>
  	<tr>
        <td>
          <div align="center">
           <input type="submit" value="Register">
          </div>
        </td>
     </form>
   
    </table>

</div> 
<!-- END of slider -->
  <div class="cleaner h50"></div>
  <div class="cleaner"></div>
</div> 


<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.html">Home</a></li>
                <li><a href="radio.html">Radio</a></li>
                <li><a href="charts.html">Charts</a></li>
                <li><a href="top10charts.html">Top10 Charts</a></li>
                <li><a href="contact.html">Contact</a></li>
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="social twitter">Twitter</a></li>
                <li><a href="#" class="social feed">Feed</a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
            <form action="#" method="get">
              <input type="text" value="Search" name="email" id="email" title="email" onfocus="clearText(this)" onblur="clearText(this)" class="newsletter_txt" />
              <input type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  />
            </form>
            <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
    </body>
</html>
