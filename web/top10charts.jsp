<%-- 
    Document   : top10charts
    Created on : 23 Apr, 2013, 2:17:41 PM
    Author     : Akshat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Music On Demand</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    </head>
<body>
        <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="MusicPlay.jsp">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
        
<div id="main_top"></div>
<div id="main">
    <h2><A href="musicplay.do?plid=19" target="_music" > Top 10 Songs</a></h2>
		<div class="gallery_box">
        	<a href="#" rel="lightbox[portfolio]"><img src="images/charts1.jpg" alt="" class="image_frame"/></a><a href="view_song.do?songID=25">1.State Of Grace</a>
          <p>Taylor Swift</p>
        </div>
        <div class="gallery_box">
        	<a href="#" rel="lightbox[portfolio]"><img src="images/charts2.jpg" alt="Image 01" class="image_frame"/></a><a href="#">2.Locked Out Of Heaven</a>
          <p>Bruno Mars</p>
        </div>
        
        <div class="gallery_box no_margin_right">
        	<a href="#" rel="lightbox[portfolio]"><img src="images/charts3.jpg" alt="Image 12" class="image_frame"/></a><a href="view_song.do?songID=26">3.As Long As You Love Me</a>
          <p>Justin Bieber</p>
        </div>
  <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts4.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">4.Titanium</a>    
    <p>David Guetta</p>
        </div>
  <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts5.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">5.Skyfall</a>
    <p>Adele.</p>
  </div>
  <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts6.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">6.Your Beautiful</a>
    <p>One Direction</p>
        </div>
        <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts7.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">7.I Cry</a>
          <p>Flo Rida</p>
  </div>
        <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts8.jpg" alt="Image 08" class="image_frame"/></a> <a href="view_song.do?songID=27">8.Just The Way You Are</a>
          <p>Bruno Mars</p>
  </div>
  <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts9.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">9.Set Fire To Rain</a>
    <p>Adele</p>
        </div>
        <div class="gallery_box"> <a href="#" rel="lightbox[portfolio]"><img src="images/charts10.jpg" alt="Image 08" class="image_frame"/></a> <a href="#">10.Diamonds</a>
          <p>Rihanna</p>
  </div>
        
        <div class="cleaner"></div> 
    <div class="cleaner"></div>
</div> 

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                    <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
    </body>
</html>
