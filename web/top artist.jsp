<%-- 
    Document   : top artist
    Created on : 23 Apr, 2013, 2:16:54 PM
    Author     : Akshat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Music On Demand</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                }else{out.println("&nbsp;");} out.flush(); %></a> </li>
                
                <li><%if(session.getAttribute("email_id")!=null){ %>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>
        

<div id="main_top"></div>
<div id="main">
	
    <div id="content" class="float_l">
   		<div class="post">
            	<img src="images/A.R.rehman artist.jpg" alt="Image 01" width="350" />
          <h3><strong>A R Rehman</strong></h3>
          <h5><a href="MusicPlay.jsp">Maa Tujhe Salaam - Vande Maataram</a></h5>
          <h5><a href="MusicPlay.jsp">Jai Ho - Slumdog Millionaire</a></h5>
          <h5><a href="MusicPlay.jsp">Tu Hi Re - Bombay</a></h5>
          <h5><a href="MusicPlay.jsp">Paathshala - Rang De Basanti</a></h5>
          <h5><a href="MusicPlay.jsp">Oh Hum Dum - Saathiya</a></h5>
          <h5><a href="MusicPlay.jsp">Khalbali Hai Khalbali - Rang De Basanti</a></h5>
          <div class="cleaner"></div>
          <div class="meta">
                   <p>&nbsp;</p>
                   <div class="cleaner"></div>
                </div>
            	 <div class="cleaner"></div>
	  </div>
      <div class="post">
            	<img src="images/snou nigam artist.jpg" alt="Image 01" width="350" />
          <h3><strong>Sonu Nigam</strong></h3>
          <h5><a href="MusicPlay.jsp">Tanhayee - Dil Chahta Hai</a></h5>
          <h5><a href="MusicPlay.jsp">Diwana Dil - Pardes</a></h5>
          <h5><a href="MusicPlay.jsp">Do pal - Veer Zaara</a></h5>
          <h5><a href="MusicPlay.jsp">Sun Zara - Lucky</a></h5>
          <h5><a href="MusicPlay.jsp">Mujhe Raat Din - Deewana</a></h5>
          <h5><a href="MusicPlay.jsp">Aal Izz Well - 3 idiots</a></h5>
          <div class="cleaner"></div>
          <div class="meta">
                   <p>&nbsp;</p>
                   <div class="cleaner"></div>
        </div>
            	 <div class="cleaner"></div>
	  </div>
      <div class="post">
            	<img src="images/Shreya Ghosal top artist.jpg" alt="Image 01" width="350" />
          <h3><strong>Shreya Ghoshal</strong></h3>
          <h5><a href="MusicPlay.jsp">Radha - Student Of The Year</a></h5>
          <h5><a href="MusicPlay.jsp">Teri Meri - Bodyguard</a></h5>
          <h5><a href="MusicPlay.jsp">Jhalla Wallah - Ishaqzaade</a></h5>
          <h5><a href="MusicPlay.jsp">Mashallah - Ek Tha Tiger</a></h5>
          <h5><a href="MusicPlay.jsp">Radha - Student Of The Year</a></h5>
          <h5><a href="MusicPlay.jsp">Saans - Jab Tak Hai Jaan</a></h5>
          <div class="cleaner"></div>
          <div class="meta">
                   <p>&nbsp;</p>
                   <div class="cleaner"></div>
        </div>
            	 <div class="cleaner"></div>
	  </div>
      <div class="post">
            	<img src="images/Mohit Chauhan top artist.jpg" alt="Image 01" width="350" />
          <h3><strong>Mohit Chauhan</strong></h3>
          <h5><a href="MusicPlay.jsp">Jo Bhi Main - Rockstar</a></h5>
          <h5><a href="MusicPlay.jsp">Ye Dooriyan - Love Aajkal</a></h5>
          <h5><a href="MusicPlay.jsp">Anjaana Anjaani - Tujhe Bhula Diya </a></h5>
          <h5><a href="MusicPlay.jsp">Tum Ho - Rockstar</a></h5>
          <div class="cleaner"></div>
          <div class="meta">
                   <p>&nbsp;</p>
                   <div class="cleaner"></div>
        </div>
            	 <div class="cleaner"></div>
	  </div>
    </div>
    <div class="cleaner"></div>
</div> <!-- END of main -->

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                    <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
    </body>
</html>
