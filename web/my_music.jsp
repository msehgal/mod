<%-- 
    Document   : my_music
    Created on : Mar 24, 2013, 9:25:19 PM
    Author     : Mohit
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>My Music</title>
		<link href="CSS/mymusic.css" rel="stylesheet" type="text/css" />
		<link href="CSS/style1.css" rel="stylesheet" type="text/css" />
		<link href="CSS/style_mod.css" rel="stylesheet" type="text/css" />
        <link href="CSS/ddsmoothmenu.css" rel="stylesheet" type="text/css">
    </head>
    <body>
<%
    if(session.getAttribute("name")==null)
    {
        response.sendRedirect("/MOD/home.jsp");
    }
                String email_id=(String)session.getAttribute("email_id");
                String user_name=(String)session.getAttribute("name");
                String userID=(String)session.getAttribute("userID");
                System.out.println("userID"+userID);
                //queries
                Connection con=DBConnector.getConnection();
                Statement stmt;
                ResultSet songPlaylist;
                String current_playlistID;
                 String list_playlist="select * from playlist where playlistID in (select playlistID from user_owns_playlist  where userID="+userID+" );";
                stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
               
                ResultSet listPlaylist_rs=stmt.executeQuery(list_playlist);
                
                listPlaylist_rs.first();
                if(request.getParameter("plid")==null&&listPlaylist_rs.next())
                {current_playlistID=listPlaylist_rs.getString("playlistID");
                listPlaylist_rs.previous();}
                else 
                {
                    current_playlistID=request.getParameter("plid");
                }
                 if(request.getParameter("del")!=null&&request.getParameter("del").equals("true"))
                 {
                     if(listPlaylist_rs!=null&&listPlaylist_rs.first()&&listPlaylist_rs.next())
                        current_playlistID=listPlaylist_rs.getString("playlistID");
                 }
                
%>    

<div id="header_wrapper">
    <div id="header">
        <div id="site_title"><a href="#">MUSIC ON DEMAND</a></div>
        <div class="ddsmoothmenu" id="menu">
            <ul>
                <li><a href="home.jsp" class="selected">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.do">Charts</a></li>
                <li><a href="Music.jsp">Music</a></li>
                  <li> <%if(session.getAttribute("email_id")==null){%>
                      <a href="login.jsp"> login</a><% }%> </li>
                <li><a><%if(session.getAttribute("email_id")!=null){ out.print(session.getAttribute("name"));
                        System.out.println("name  "+session.getAttribute("email_id"));
                 %></a> </li>
                
                <li>
                    <html:link action="/logout" >Logout </html:link>
                    <% } %></li>
            </ul>
        </div> 
    </div> 
</div>

<div id="main">
	
<div id="playlist_list" >
<table>
<%
    int count=0;
    stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    String song_playlist="select art.artist_name,pl2.* from artist art,(select main.*,pl.* from playlist pl, (select * from song where songID in (select songID from [dbo].playlist_contains_song where playlistID='"+current_playlistID+"')) main where pl.playlistID='"+current_playlistID+"') pl2 where pl2.songID=art.songID";
              
    listPlaylist_rs.first();
     
    while(listPlaylist_rs.next())
    {
      
 %>
 <tr><td><a  href="my_music.jsp?plid=<%=listPlaylist_rs.getString("playlistID")%>"><%=listPlaylist_rs.getString("playlist_name")%></a></td></tr>
<%
    }
%>
 <tr><td><form method="post" action="createplaylist.do" ><input type="text" size="15" name="playlist_name" /><input type="submit" value=" Create Playlist+"/></form></td></tr>
</table>
</div>
<div class="playlist_header">
<%
    System.out.println("query "+song_playlist);
        songPlaylist=stmt.executeQuery(song_playlist);
    boolean isemp=true;
    if(songPlaylist!=null)
    {
        
         isemp=!songPlaylist.next();
         if(isemp==false)
             songPlaylist.previous();
    }
   System.out.println("isemp "+isemp);
       
%>
<b>&nbsp;&nbsp;<%if (!isemp){ songPlaylist.next();%><%=songPlaylist.getString("playlist_name")%><%}%></b><br />
<form action="deletefrmplaylist.do?plid=<%=current_playlistID%>" method="post">
<table>
<tr><%if(!isemp){%>
    <td>&nbsp;</td><td><a target="_music" href="musicplay.do?plid=<%=songPlaylist.getString("playlistID")%>">Play</a></td><td><a>Add Songs</a></td><td><a ><input type="submit" value="Remove Songs" /></a></td><td><a href="delete_playlist.do?plid=<%=current_playlistID%>&del=true" onclick=""> Delete Playlist</a> </td>
<%}%>
</tr>
</table>
<br />
</div>
<div class="playlist_content">
<table>
        <%if(!isemp){%>
	<tr><th max-width="50px"></th><th>Title</th><th>Artist</th><th>Album</th><th>On Your Mind</th><th>Dedicate</th><th>Recommend</th></tr>
	<%}else{%>
        <b>No Songs in the list.</b>
        <%}
        
             if(songPlaylist!=null)
                 songPlaylist.first();
        while(songPlaylist.next())
        {
        %>
        <tr><td max-width="50px"><input type="checkbox" name="select_songs" value="<%=songPlaylist.getString("songID")%>"  />  </td>
            <td><%=songPlaylist.getString("name")%></td><td><%=songPlaylist.getString("artist_name")%> </td><td><%=songPlaylist.getString("album")%></td><td>On Your Mind</td><td>Dedicate</td><td>Recommend</td></tr>
        
        <%}%>
</table>

</div>
</form>
</div>
<div class="cleaner"></div>

<div id="footer_wrapper">
	<div id="footer">
	
		<div class="col col_14">
        	<h5><a href="#" class="footer_list">FEEDBACK</a></h5>
            <ul class="footer_list">
            	<li><a href="my_home.jsp">User Home</a></li>
                <li><a href="my_music.jsp">User Music</a></li>
                <li><a href="sign_up.jsp">Sign Up</a></li>
                <li><a href="MusicPlay.jsp">Music Player</a></li>
          </ul>
            
      </div>
        <div class="col col_14">
        	<h5>Pages</h5>
            <ul class="footer_list">
            	<li><a href="home.jsp">Home</a></li>
                <li><a href="radio.jsp">Radio</a></li>
                <li><a href="charts.jsp">Charts</a></li>
                <li><a href="top10charts.jsp">Top10 Charts</a></li>
                
			</ul>
        </div>
        <div class="col col_14">
        	<h5>Follow Us</h5>	
            <ul class="footer_list">
                <li><a href="http://www.facebook.com/MusicOdemand" class="social facebook">
               <div class="fb-like" data-href="http://www.facebook.com/MusicOdemand" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="lucida grande"></div></a></li>
               <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    </script>
                <li><a href="#" class="Google Plus">
                    <div class="g-plusone"></div>
                    <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script></a></li>
			</ul>
            
        </div>
        
        <div class="col col_14 no_margin_right">
        	<h5>Search Site</h5>
                <button type="submit" name="Search" value=" Search " alt="Subscribe" id="subscribe_button" title="Search" class="subscribe_button"  /><a href="song_search.jsp">Search</a> </button>
                
          <div class="cleaner h30"></div>
            <p>Copyright © 2013 MOD Team</p>
        </div>
        
    <div class="cleaner"></div>
    </div>
</div> 
</body>
</html>
