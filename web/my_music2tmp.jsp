<%-- 
    Document   : my_music
    Created on : Mar 24, 2013, 9:25:19 PM
    Author     : Mohit
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="database.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>My Music</title>
<link href="CSS/mymusic.css" rel="stylesheet" type="text/css" />
<link href="CSS/style1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
<%
    if(session.getAttribute("name")==null)
    {
        response.sendRedirect("/MOD/radio_home.jsp");
    }
                String email_id=(String)session.getAttribute("email_id");
                String user_name=(String)session.getAttribute("name");
                String userID=(String)session.getAttribute("userID");
                System.out.println("userID"+userID);
                //queries
                Connection con=DBConnector.getConnection();
                Statement stmt;
                ResultSet songPlaylist;
                String current_playlistID;
                 String list_playlist="select * from playlist where playlistID in (select playlistID from user_owns_playlist  where userID="+userID+" );";
                stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
               
                ResultSet listPlaylist_rs=stmt.executeQuery(list_playlist);
                
                listPlaylist_rs.first();
                if(request.getParameter("plid")==null&&listPlaylist_rs.next())
                {current_playlistID=listPlaylist_rs.getString("playlistID");}
                else 
                {
                    current_playlistID=request.getParameter("plid");
                }
                 if(request.getParameter("del")!=null&&request.getParameter("del").equals("true"))
                 {
                     if(listPlaylist_rs!=null&&listPlaylist_rs.first()&&listPlaylist_rs.next())
                        current_playlistID=listPlaylist_rs.getString("playlistID");
                 }
                
%>    

        <div class="base_all" >
	<div class="top_navigation_bar">
			<div class="top_navigation_bar_element"><a href="#">Search</a></div>
			<div class="top_navigation_bar_element"><a href="#">Radio</a></div>
			<div class="top_navigation_bar_element"><a href="#">Charts</a></div>
			<div class="top_navigation_bar_element"><a href="#">MyMusic</a></div>
			<div class="top_navigation_bar_element"><a href="#">MusicNetwork</a></div>
	</div>
	<div class="main_content">
		<div class="logo">Search Logo</div>
	
<div id="playlist_list" >
<table>
<%
    int count=0;
    stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                 String song_playlist="select main.*,pl.* from playlist pl, (select * from song where songID in (select songID from [dbo].playlist_contains_song where playlistID='"+current_playlistID+"')) main where pl.playlistID='"+current_playlistID+"'";
              
    listPlaylist_rs.first();
     
    while(listPlaylist_rs.next())
    {
      
 %>
 <tr><td><a  href="my_music.jsp?plid=<%=listPlaylist_rs.getString("playlistID")%>"><%=listPlaylist_rs.getString("playlist_name")%></a></td></tr>
<%
    }
%>
 <tr><td><form method="post" action="createplaylist.do" ><input type="text" size="15" name="playlist_name" /><input type="submit" value=" Create Playlist+"/></form>></td></tr>
</table>
</div>
<div class="playlist_header">
<%
        songPlaylist=stmt.executeQuery(song_playlist);
    boolean isemp=false;
    if(songPlaylist!=null)   
     isemp=songPlaylist.next();
     if(songPlaylist!=null)
             { 
                 songPlaylist.first();
             }
%>
<b>&nbsp;&nbsp;<%if (isemp){%><%=songPlaylist.getString("playlist_name")%><%}%></b><br />
<form action="deletefrmplaylist.do?plid=<%=current_playlistID%>" method="post">
<table>
<tr>
    <td>&nbsp;</td><td><a href="musicplay.do?plid=<%=songPlaylist.getString("playlistID")%>">Play</a></td><td><a>Add Songs</a></td><td><a ><input type="submit" value="Remove Songs" /></a></td><td><a href="delete_playlist.do?plid=<%=current_playlistID%>&del=true" onclick=""> Delete Playlist</a> </td>
</tr>
</table>
<br />
</div>
<div class="playlist_content">
<table>
	<tr><th max-width="50px"></th><th>Title</th><th>Artist</th><th>Album</th><th>On Your Mind</th><th>Dedicate</th><th>Recommend</th></tr>
	 <%
             if(songPlaylist!=null)
             {         songPlaylist.first();
        while(songPlaylist.next())
        {
        %>
        <tr><td max-width="50px"><input type="checkbox" name="select_songs" value="<%=songPlaylist.getString("songID")%>"  />  </td>
            <td><%=songPlaylist.getString("name")%></td><td>Himesh Reshammiya</td><td><%=songPlaylist.getString("album")%></td><td>On Your Mind</td><td>Dedicate</td><td>Recommend</td></tr>
        
        <%}}%>
</table>

</div>
    </form>

 </div>
<div class="footer">Playlist Controls will Come here.</div>
</div>
</body>
</html>
