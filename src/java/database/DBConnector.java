/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author I
 */
public class DBConnector {
    public static Connection con;
    public String url;
    public static DBConnector dbcon;
    private DBConnector() throws SQLException
    {
        DriverManager.registerDriver( new com.microsoft.sqlserver.jdbc.SQLServerDriver());
           url="jdbc:sqlserver://127.0.0.1:1433;databaseName=music;";
            //   String url2="jdbc:sqlserver://SEHGAL-PC\\SQLEXPRESS;databaseName=music;";
           
            con=DriverManager.getConnection(url,"sa","capstone");
    }
    public static Connection getConnection() throws SQLException
    {
        dbcon=new DBConnector();
        return con;
    }
    public static void closeConnection() throws SQLException
    {
        con.close();
    }
}
