/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class CreatePlaylistAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
            String playlist_name=request.getParameter("playlist_name");
            HttpSession session=request.getSession(false);
            String userID=(String) session.getAttribute("userID");
            //connection
            Connection con= DBConnector.getConnection();
            Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //queries
            String addPlaylist_query="INSERT INTO [music].[dbo].[playlist]([playlist_name]) VALUES('"+playlist_name+"')";
            String playlistID="";
            int res=stmt.executeUpdate(addPlaylist_query, Statement.RETURN_GENERATED_KEYS);
            ResultSet playlistID_rs=stmt.getGeneratedKeys();
            playlistID_rs.next();
            playlistID=""+playlistID_rs.getLong(1);
            System.out.println("playlist id generated= "+playlistID);
            String addPlaylistToUser_query="INSERT INTO [music].[dbo].[user_owns_playlist]([userID],[playlistID],[time_owned])VALUES('"+userID+"','"+playlistID+"',sysdatetime())";
            Statement stmt2=con.createStatement();
            
          int res2=  stmt2.executeUpdate(addPlaylistToUser_query);
          
            return mapping.findForward(SUCCESS);
    }
}
