/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class MusicPlayAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
               String playlistID=(String)request.getParameter("plid");
                    
                HttpSession session=request.getSession(false);
                if(session==null&& !playlistID.equals("1")&& !playlistID.equals("charts"))
                {
                    return mapping.findForward("fail");
                }
                else
                {
                    Connection con= DBConnector.getConnection();
                    Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    String plFetchQuery;
                    if(playlistID.equals("charts"))
                    {
                        plFetchQuery="select * from [dbo].[charts]";
                    }
                    else
                    {
                        plFetchQuery="select sng.*,art.artist_name from artist art, (select p.playlistID,s.* from song s left outer join playlist_contains_song p on s.songID=p.songID where p.playlistID='"+playlistID+"') sng  where sng.songID=art.songID;";
                    }
                    ResultSet currPlaylist_rs=stmt.executeQuery(plFetchQuery);
                    session.setAttribute("currPlaylist_rs", currPlaylist_rs);
                    return mapping.findForward(SUCCESS);
    
                }
    }
}
