/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class ViewSongAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String FAILURE="failure";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
            String songID=request.getParameter("songID");
            Connection con=DBConnector.getConnection();
            Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
            Statement stmt2=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
            Statement stmt3=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
        
            ResultSet song=stmt.executeQuery("select * from song where songID="+songID);
        
            ResultSet artist=stmt2.executeQuery("select * from artist where songID="+songID);
            System.out.println("songID in viewAction"+songID);
            ResultSet review=stmt3.executeQuery("select * from song_review sr inner join [dbo].[user] usr on sr.userID=usr.userID WHERE sr.songID="+songID);
            if(song==null)
            {
            
               return mapping.findForward(FAILURE);
            }
            else
            {
              if(song.next())
              {
                    request.setAttribute("viewSongName",song.getString("name"));
                  System.out.println(song.getString("name"));
                    song.first();
              }
              
                  request.setAttribute("viewSongID",songID);
                 request.setAttribute("viewSong_rs", song);
                   
                 request.setAttribute("viewSongArtist_rs", artist);
                 request.setAttribute("viewSongReview_rs", review);
                  return mapping.findForward(SUCCESS);
            }
       
    }
}
