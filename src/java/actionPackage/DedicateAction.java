/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class DedicateAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
         HttpSession session=request.getSession(false);
        String songID=request.getParameter("songID");
        String src=request.getParameter("src");
        String frndID=request.getParameter("frnusr");
        if(session==null)
        {
            return mapping.findForward(src);
        }
        else
        {
            String userID= (String)session.getAttribute("userID");
            if(userID==null)
            { 
                return mapping.findForward(src);
            }
            else
            {
                Connection con= DBConnector.getConnection();
                Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                String dedicateQuery="INSERT INTO [music].[dbo].[user_dedicates_song]([userID],[songID],[friendID],[time_dedicated]) VALUES('"+userID+"','"+songID+"','"+frndID+"',SYSDATETIME())";
                int res=stmt.executeUpdate(dedicateQuery);
                return mapping.findForward(src);
            }
        }
    }
}
