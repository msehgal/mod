/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.eclipse.persistence.sessions.Session;

/**
 *
 * @author MOHIT
 */
public class SignUpAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String fname,lname,email,pwd,gnd;
        fname=request.getParameter("fname");
        lname=request.getParameter("lname");
        email=request.getParameter("email");
        pwd=request.getParameter("pwd");
        gnd=request.getParameter("gender");
        String if_exist="SELECT COUNT(*) as exist FROM [music].[dbo].[user] where email_id='"+email+"'",adduser_query="";
        Connection con= DBConnector.getConnection();
        Statement stmt=con.createStatement(/*ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE*/),stmt2;
        ResultSet ifexist_rs=stmt.executeQuery(if_exist);
        HttpSession session= request.getSession(true);
        ifexist_rs.next();
        String ifexist=ifexist_rs.getString("exist");
        System.out.println("ifexist "+ifexist);
        if(!ifexist.equals("0"))
        {
           session.setAttribute("exists", "yes");
           session.setAttribute("add_user_status", "failed");
           session.setAttribute("failed_mail", email);
            
        }
        else
        {
            stmt2=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            session.setAttribute("exists", "no");
            adduser_query="INSERT INTO [music].[dbo].[user]   ([user_name],[password],[email_id],[gender])  VALUES('"+(fname+" "+lname).trim()+"','"+pwd+"','"+email+"','"+gnd+"')";
            System.out.println("add user query "+adduser_query);
            int result=stmt2.executeUpdate(adduser_query);
            
            if(result==1)
            {
                session.setAttribute("add_user_status", "success");
            }
            else
            {
                session.setAttribute("add_user_status", "failed");
               // session.setAttribute("failed_mail", email);
            }
        }
        
        return mapping.findForward(SUCCESS);
    }
}
