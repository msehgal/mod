/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class CommentAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session=request.getSession();
        String comment= request.getParameter("comment");
        System.out.println("comment in comment action"+comment);
        System.out.println("viewsongID in comment action"+request.getAttribute("viewSongID"));
        Connection con=DBConnector.getConnection();
        Statement stmt=con.createStatement();
        String userID=null;
        if(session.getAttribute("userID")==null)
        {
            userID="-1";
        }
        else
        {
            userID=(String)session.getAttribute("userID");
        }
        String query="INSERT INTO [music].[dbo].[song_review]([reviewText],[songID],[userID])VALUES('"+comment+"' ,'"+request.getAttribute("viewSongID")+"',"+userID+ " );";
        stmt.executeUpdate(query);
       
    
        return mapping.findForward(SUCCESS);
    }
}
