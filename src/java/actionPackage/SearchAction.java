/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class SearchAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
            Connection con= DBConnector.getConnection();
           String search_box=request.getParameter("search_box");
            Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String searchQuery="SELECT * FROM [music].[dbo].[song] WHERE [name] like '%"+search_box+"%' ; ";
            
            System.out.println("submit_box"+search_box);
           // PreparedStatement pstmt=con.prepareStatement(searchQuery, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //pstmt.setString(1, search_box);
            ResultSet searchName_rs=stmt.executeQuery(searchQuery);
           request.setAttribute("searchName_rs", searchName_rs);
           searchName_rs.next();
        System.out.println("searched name"+searchName_rs.getString("name"));
            //DBConnector.closeConnection();
        return mapping.findForward(SUCCESS);
    }
}
