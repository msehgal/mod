/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class SetOnMindAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session=request.getSession(false);
        String songID=request.getParameter("songID");
        String src=request.getParameter("src");
        if(session==null)
        {
            return mapping.findForward(src);
        }
        else
        {
            String userID= (String)session.getAttribute("userID");
            if(userID==null)
            { 
                return mapping.findForward(src);
            }
            else
            {
                Connection con= DBConnector.getConnection();
                Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                String setonmindQuery="UPDATE [music].[dbo].[user] SET [songID] ='"+songID+"'  WHERE userID='"+userID+"'";
                int res=stmt.executeUpdate(setonmindQuery);
                return mapping.findForward(src);
            }
        }
    }
}
