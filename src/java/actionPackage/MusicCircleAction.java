/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package actionPackage;

import database.DBConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author MOHIT
 */
public class MusicCircleAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session=request.getSession(false);
        if(session==null)
        {
            return mapping.findForward("fail");
        }
        else
        {
             String userID=(String) session.getAttribute("userID");
             ResultSet friendList_rs=(ResultSet)session.getAttribute("friendList_rs");
             if(userID==null||friendList_rs==null)
                       return mapping.findForward("fail");
               
//             System.out.println("session userID"+userID);
//                String circle="select usr2.[user_name] as uname2,usr2.songID as onMind2,base2.* from (select basefrnd.*,usr.[user_name] as uname1,usr.songID as onMind1 from (select * from dbo.user_friend_of_user where (user_friend_of_user.friendID1="+userID+" or user_friend_of_user.friendID2="+userID+") and user_friend_of_user.[status]=3 ) basefrnd join dbo.[user] usr on basefrnd.friendID1=usr.userID) base2 join dbo.[user] usr2 on base2.friendID2=usr2.userID;";
//             System.out.println("query executed"+circle);
//             Connection con=DBConnector.getConnection();
//             Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//              ResultSet circle_rs=stmt.executeQuery(circle);
//             request.setAttribute("circle_rs", circle_rs);
             return mapping.findForward(SUCCESS);
        }
    }
}
