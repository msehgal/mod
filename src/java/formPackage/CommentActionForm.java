/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formPackage;

import java.util.Enumeration;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.MessageResources;

/**
 *
 * @author MOHIT
 */
public class CommentActionForm extends org.apache.struts.action.ActionForm {
    
    private String comment;


    /**
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param string
     */
    public void setComment(String string) {
       comment = string;
    }

   
    /**
     *
     */
    public CommentActionForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
      //  MessageResources msgResource;
        //msgResource = MessageResources.getMessageResources("ApplicationResource.properties");
       // System.out.println(new ActionMessage("song.default.comment"));    
        System.out.println("viewsongID in comment action form");
       Enumeration enumer;
        enumer = request.getAttributeNames();
       while(enumer.hasMoreElements())
       {
            String next =(String) enumer.nextElement();
            System.out.println("attribue  "+next+"  value  "+request.getAttribute(next));
       }
        if (getComment() == null || getComment().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        else if(getComment().equals("leave your comment"))
        {
            
        }
        return errors;
    }
}
