/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package formPackage;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author MOHIT
 */
public class SearchActionForm extends org.apache.struts.action.ActionForm {
    
    private String submit_button;
    private String search_box;

    /**
     * @return
     */
    public String getSubmit_button() {
        return submit_button;
    }

    /**
     * @param string
     */
    public void setSubmit_button(String string) {
        submit_button = string;
    }

    /**
     * @return
     */
    public String getSearch_box() {
        return search_box;
    }

    /**
     * @param i
     */
    public void setSearch_box(String i) {
        search_box = i;
    }

    /**
     *
     */
    public SearchActionForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getSearch_box()== null || getSubmit_button().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
}
